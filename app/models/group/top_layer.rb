# encoding: utf-8

class Group::TopLayer < ::Group

  self.layer = true

  children Group::Circle

  ### ROLES
  class Leader < ::Role
    self.permissions = [:layer_and_below_full, :admin]
  end

  class Member < ::Role
    self.permissions = [:group_read]
  end

  class Guest < ::Role
    self.permissions = []
  end

  roles Leader, Member, Guest

end
