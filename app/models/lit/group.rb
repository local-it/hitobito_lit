# encoding: utf-8

module Lit::Group
  extend ActiveSupport::Concern

  included do
    # Define additional used attributes
    # self.used_attributes += [:website, :bank_account, :description]
    # self.superior_attributes = [:bank_account]

    ::Group.root_types(::Group::TopLayer)
  end

end
