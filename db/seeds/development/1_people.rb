# frozen_string_literal: true
# encoding: utf-8

require Rails.root.join('db', 'seeds', 'support', 'person_seeder')

class LitPersonSeeder < PersonSeeder

  def amount(role_type)
    case role_type.name.demodulize
    when 'Member' then 5
    else 1
    end
  end

end

devs = {
  'Philipp Rothmann' => 'p.rothmann@local-it.org'
}

seeder = LitPersonSeeder.new

seeder.seed_all_roles

root = Group.root
devs.each do |name, email|
  seeder.seed_developer(name, email, root, Group::TopLayer::Leader)
end
